#include<iostream>
#include<cmath>

#include<SFML/Window.hpp>
#include<SFML/Graphics.hpp>
#include<SFML/System.hpp>

#include"../include/runner.hpp"

float dist(float a, float b);

int main(){
    Runner app{800, 600, "Lab01"};
    if(app.init())
        return app.run();
    else
        return 0;
}



float dist(float a, float b){
    return sqrt(a*a + b*b);
}