#include"../include/runner.hpp"

Runner::Runner(int _resX, int _resY, const std::string &title, int desired_fps):
window{sf::VideoMode{static_cast<unsigned>(_resX), static_cast<unsigned>(_resY)}, title, sf::Style::Titlebar | sf::Style::Close},
                draw_color{125, 125, 125}, bg_color{29, 29, 29},
                window_title{title}, fps{desired_fps},
                resX{_resX}, resY{_resY},
                state{IDLE}, mouse_is_pressed{false} {}
                
Runner::~Runner() {

}

bool Runner::init() {
    window.setFramerateLimit(fps);
    texture.create(resX, resY);
    texture.clear(sf::Color::Black);
    texture.display();
    return true;
}
int Runner::run() {

    while(window.isOpen()) {
        window.clear(sf::Color::Transparent);

        event_handler();
        window.draw(sf::Sprite(texture.getTexture()));
        if(mouse_is_pressed)
            draw_current_shape();




        // texture.display();
        draw_gui();
        window.display();
        
    }

    return 0;
}

void Runner::event_handler() {

        while(window.pollEvent(event)) {

            switch (event.type) {
                case sf::Event::KeyPressed:

                    switch (event.key.code) {
                        case sf::Keyboard::Escape:
                            window.close();
                            break;
                        case sf::Keyboard::F:
                            state = CHOSING_DRAW_COLOR;
                            break;
                        case sf::Keyboard::B:
                            state = CHOSING_BG_COLOR;
                            break;
                        case sf::Keyboard::L:
                            state = DRWNG_LINE;
                            break;
                        case sf::Keyboard::R:
                            state = DRWNG_RECT;
                            break;
                        case sf::Keyboard::A:
                            state = DRWNG_RECT_FILL;
                            break;
                        case sf::Keyboard::C:
                            state = DRWNG_CRCL;
                            break;
                        case sf::Keyboard::W:
                            if(!save_image()) {
                                std::cerr << "Saving file unsuccessfull!\n";
                            }
                            break;
                        case sf::Keyboard::O:
                            if(!load_image()) {
                                std::cerr << "Loading file unsuccessfull!\n";
                            }
                            break;
                        default:
                            break;
                    }
                    break;

                case sf::Event::MouseButtonPressed:

                    mouse_pressed();

                    break;
                case sf::Event::MouseMoved:

                    mouse_move();

                    break;
                case sf::Event::MouseButtonReleased:

                    mouse_released();

                    break;
                default:
                    break;
            }

        }

}

void Runner::mouse_pressed() {
    
    
    if(!mouse_is_pressed) {
        mouse_dragX = event.mouseButton.x;
        mouse_dragY = event.mouseButton.y;
        mouse_is_pressed = true;
    }

    switch(state) {
        case CHOSING_BG_COLOR:

            if((mouseX > 0) && (mouseX < (resX * .95f)) && (mouseY > 0) && (mouseY < (resY * .1f))){
                bg_color = pick_palette_color();
                state = IDLE;
            }
            break;
        case CHOSING_DRAW_COLOR:
            if((mouseX > 0) && (mouseX < (resX * .95f)) && (mouseY > 0) && (mouseY < (resY * .1f))){
                draw_color = pick_palette_color();
                state = IDLE;
            }
            break;
        default:
            break;
    }
}
void Runner::mouse_move() {
    mouseX = event.mouseMove.x;
    mouseY = event.mouseMove.y;
}
void Runner::mouse_released() {
    draw_current_shape(true);
    mouse_is_pressed = false;
    state = IDLE;
}

void Runner::draw_current_shape(bool finalize) {
    if(state > 2) {
    switch (state) {
        case DRWNG_CRCL:{
            sf::CircleShape c;
            c.setRadius(dist(mouse_dragX - mouseX, mouse_dragY - mouseY));
            c.setPosition(sf::Vector2f{static_cast<float>(mouse_dragX - c.getRadius()),
                                        static_cast<float>(mouse_dragY - c.getRadius())});
            c.setOutlineColor(draw_color);
            c.setOutlineThickness(3.f);
            c.setFillColor(sf::Color::Transparent);

            if(!finalize)
                window.draw(c);
            else{
                texture.draw(c);
            }
            break;
        } case DRWNG_LINE:{
            sf::VertexArray l{sf::LinesStrip, 2};
            l[0].position = sf::Vector2f{static_cast<float>(mouse_dragX), static_cast<float>(mouse_dragY)};
            l[1].position = sf::Vector2f{static_cast<float>(mouseX), static_cast<float>(mouseY)};
            l[0].color = draw_color;
            l[1].color = bg_color;

            if(!finalize)
                window.draw(l);
            else{
                texture.draw(l);
            }
            break;
        } case DRWNG_RECT:{
            sf::RectangleShape r;
            r.setPosition(sf::Vector2f{static_cast<float>(mouse_dragX), static_cast<float>(mouse_dragY)});
            r.setSize(sf::Vector2f{static_cast<float>(mouseX - mouse_dragX), static_cast<float>(mouseY - mouse_dragY)});
            r.setOutlineColor(draw_color);
            r.setOutlineThickness(1.f);
            r.setFillColor(sf::Color::Transparent);

            if(!finalize)
                window.draw(r);
            else{
                texture.draw(r);
            }
            break;
        } case DRWNG_RECT_FILL:{
            sf::RectangleShape f;
            f.setPosition(sf::Vector2f{static_cast<float>(mouse_dragX), static_cast<float>(mouse_dragY)});
            f.setSize(sf::Vector2f{static_cast<float>(mouseX - mouse_dragX), static_cast<float>(mouseY - mouse_dragY)});
            f.setOutlineColor(draw_color);
            f.setOutlineThickness(1.f);
            f.setFillColor(bg_color);

            if(!finalize)
                window.draw(f);
            else{
                texture.draw(f);
            }
            break;
        } default:
            break;
    }
    }
}

void Runner::draw_gui() {
    // drawing two color palettes on top of the screen
    sf::VertexArray line{sf::LinesStrip, 2};
    sf::Color strip;
    for(int i=0; i<(resX * .95f); ++i) {
        strip = hsv2rgb( map(i, 0, resX * .95f, 180, 0) );
        line[0].position.x = i;
        line[0].position.y = 0;
        line[0].color = strip;
        line[1].position.x = i;
        line[1].position.y = resY * .05;
        line[1].color = strip;

        window.draw(line);

        strip = hsv2rgb( map(i, 0, resX * .95f, 180, 360) );
        line[0].position.y = resY * .05;
        line[0].color = strip;
        line[1].position.y = resY * .1;
        line[1].color = strip;

        window.draw(line);
    }

    // drawing current color indicators

    sf::RectangleShape rect;
    rect.setOutlineColor(sf::Color::Black);
    rect.setOutlineThickness(2.f);
    rect.setSize(sf::Vector2f{resX * .05f, resY * .05f});

    rect.setPosition(sf::Vector2f{resX * .95f - 1, resY * 0.f});
    rect.setFillColor(draw_color);
    window.draw(rect);

    rect.setPosition(sf::Vector2f{resX * .95f - 1, resY * .05f});
    rect.setFillColor(bg_color);
    window.draw(rect);

    // outline of drawing area

    sf::RectangleShape drawing_area;
    drawing_area.setSize(sf::Vector2f{resX - 2.f, resY * .8f});
    drawing_area.setPosition(sf::Vector2f{1, resY * .1f});
    drawing_area.setOutlineColor(sf::Color::White);
    drawing_area.setOutlineThickness(1.f);
    drawing_area.setFillColor(sf::Color::Transparent);

    window.draw(drawing_area);

    // writing out options in the bottom part of the screen

}

bool Runner::load_image() {

    sf::Texture tex;
    if(tex.loadFromFile("screenshot.png")) {
        sf::Sprite spr{tex};
        texture.draw(spr);
        return true;
    }
    return false;
}
bool Runner::save_image() {

    const sf::Image image{texture.getTexture().copyToImage()};
    return image.saveToFile("screenshot.png");

}
float Runner::dist(float a, float b){
    return sqrt(a*a + b*b);
}
double Runner::map(double v, double e1, double e2, double m1, double m2){
    double ret;

    if(e2 == e1)
        ret = 0;
    else{
        ret=(m2 - m1)*(v - e1)/(e2 - e1) + m1;
    }
    // std::cout << v << " mapped to " << ret << '\n';
    return ret;
}

sf::Color Runner::pick_palette_color(){
    if((mouseX > 0) && (mouseX < (resX * .95f)) && (mouseY > (resY * .05f)) && (mouseY < (resY * .1f))){
        return hsv2rgb(map(mouseX, 0, resX * .95f, 180, 360));
    } else if((mouseX > 0) && (mouseX < (resX * .95f)) && (mouseY > 0) && (mouseY < (resY * .05f))) {
        return hsv2rgb(map(mouseX, 0, resX * .95f, 180, 0));
    }
    return sf::Color::Red;  // warning plug
}

sf::Color Runner::hsv2rgb(double hue) {
    sf::Vector3f in{1., 1., 1.};
    double      hh, p, q, t, ff;
    long        i;
    sf::Vector3f   out{0., 0., 0.};

    if(in.y <= 0.0) {       // < is bogus, just shuts up warnings
        out.x = in.z;
        out.y = in.z;
        out.z = in.z;
        return sf::Color{static_cast<sf::Uint8>(out.x), static_cast<sf::Uint8>(out.y), static_cast<sf::Uint8>(out.z)};
    }
    hh = hue;
    if(hh >= 360.0) hh = 0.0;
    hh /= 60.0;
    i = (long)hh;
    ff = hh - i;
    p = in.z * (1.0 - in.y);
    q = in.z * (1.0 - (in.y * ff));
    t = in.z * (1.0 - (in.y * (1.0 - ff)));

    switch(i) {
        case 0:
            out.x = in.z;
            out.y = t;
            out.z = p;
            break;
        case 1:
            out.x = q;
            out.y = in.z;
            out.z = p;
            break;
        case 2:
            out.x = p;
            out.y = in.z;
            out.z = t;
            break;

        case 3:
            out.x = p;
            out.y = q;
            out.z = in.z;
            break;
        case 4:
            out.x = t;
            out.y = p;
            out.z = in.z;
            break;
        case 5:
        default:
            out.x = in.z;
            out.y = p;
            out.z = q;
            break;
    }
    return sf::Color{static_cast<sf::Uint8>(out.x * 255.), static_cast<sf::Uint8>(out.y * 255.), static_cast<sf::Uint8>(out.z * 255.)};
}