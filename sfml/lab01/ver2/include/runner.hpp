#ifndef _RUNNER_HPP_
#define _RUNNER_HPP_

#include<iostream>
#include<cmath>

#include<SFML/Window.hpp>
#include<SFML/Graphics.hpp>
#include<SFML/System.hpp>

enum CURRENT_ACTION {   IDLE, CHOSING_DRAW_COLOR, CHOSING_BG_COLOR,
                        DRWNG_CRCL, DRWNG_RECT, DRWNG_RECT_FILL, DRWNG_LINE, DRWNG_POINT};

class Runner{ 
private:
    sf::RenderWindow window;
    sf::RenderTexture texture;
    sf::Texture tex;
    sf::Color draw_color;
    sf::Color bg_color;
    sf::Event event;

    std::string window_title;
    int fps;

    int resX, resY;

    int mouseX, mouseY;
    int mouse_dragX, mouse_dragY;

    CURRENT_ACTION state;
    bool mouse_is_pressed;

    void event_handler();
    void mouse_pressed();
    void mouse_move();
    void mouse_released();

    void draw_current_shape(bool=false);
    void draw_gui();

    bool load_image();
    bool save_image();

    float dist(float, float);
    double map(double v, double e1, double e2, double m1, double m2);
    sf::Color pick_palette_color();
    sf::Color hsv2rgb(double hue);
public:
    Runner(int, int, const std::string&, int desired_fps = 60);
    Runner(const Runner&) = delete;
    ~Runner();

    bool init();
    int run();
};

#endif