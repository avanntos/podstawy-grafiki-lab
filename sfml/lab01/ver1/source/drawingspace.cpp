#include "../include/drawingspace.hpp"

void DrawingSpace::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	switch (drawing_mode) {
		case DRWNG_CRCL:{
			sf::CircleShape c;
			c.setRadius( dist(static_cast<float>(*mouse_dragX - *mouseX), static_cast<float>(*mouse_dragY - *mouseY)) );
			c.setPosition(sf::Vector2f{static_cast<float>(*mouse_dragX - c.getRadius()),
										static_cast<float>(*mouse_dragY - c.getRadius())});
			c.setOutlineColor(draw_color);
			c.setOutlineThickness(2.f);
			c.setFillColor(sf::Color::Transparent);
			c.setPointCount(c.getRadius() * 3);

			target.draw(c);
				
			break;
		} case DRWNG_LINE:{
			sf::VertexArray l{sf::LinesStrip, 2};
			l[0].position = sf::Vector2f{static_cast<float>(*mouse_dragX), static_cast<float>(*mouse_dragY)};
			l[1].position = sf::Vector2f{static_cast<float>(*mouseX), static_cast<float>(*mouseY)};
			l[0].color = draw_color;
			l[1].color = back_color;
			
			target.draw(l);
			break;
		} case DRWNG_RECT:{
			sf::RectangleShape r;
			r.setPosition(sf::Vector2f{static_cast<float>(*mouse_dragX), static_cast<float>(*mouse_dragY)});
			r.setSize(sf::Vector2f{static_cast<float>(*mouseX - *mouse_dragX), static_cast<float>(*mouseY - *mouse_dragY)});
			r.setOutlineColor(draw_color);
			r.setOutlineThickness(1.f);
			r.setFillColor(sf::Color::Transparent);
			
			target.draw(r);
			break;
		} case DRWNG_RECT_FILL:{
			sf::RectangleShape f;
			f.setPosition(sf::Vector2f{static_cast<float>(*mouse_dragX), static_cast<float>(*mouse_dragY)});
			f.setSize(sf::Vector2f{static_cast<float>(*mouseX - *mouse_dragX), static_cast<float>(*mouseY - *mouse_dragY)});
			f.setOutlineColor(draw_color);
			f.setOutlineThickness(1.f);
			f.setFillColor(back_color);
			
			target.draw(f);

			break;
		} default:
			break;
	}
}