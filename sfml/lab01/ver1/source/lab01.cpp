// Calosc mozna dowolnie edytowac wedle uznania. 

#include<iostream>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "../include/menu.hpp"
#include "../include/drawingspace.hpp"


/// funkcje pomocnicze sluzace do zapisu i odczytu obrazu z dysku
bool save_image(const sf::RenderTexture&);
bool load_image(sf::RenderTexture&);

int main() {
	sf::RenderWindow window(sf::VideoMode(800, 650), "GFK Lab 01", sf::Style::Titlebar | sf::Style::Close);
	sf::RenderTexture buffer;
	buffer.create(800, 650);
	buffer.clear(sf::Color::Black);
	buffer.display();
	sf::Event event;
	int mouseX = 400, mouseY = 375, mouse_drag_X = 0, mouse_drag_Y = 0;
	CURRENT_ACTION state = IDLE;
	Menu menu{&state};
	DrawingSpace ds{&mouseX, &mouseY, &mouse_drag_X, &mouse_drag_Y};
	ds.set_back_color(sf::Color::White);
	ds.set_draw_color(sf::Color::White);
	bool drawing = false;
	window.setFramerateLimit(60);

	while (window.isOpen()) {
	
		while (window.pollEvent(event)) {
			// glowny switch zarzadzajacy typem zdarzenia do obslugi w danym momencie
			switch (event.type) {
				// nacisniety klawisz na klawiaturze
				case sf::Event::KeyPressed:
					// obsluga poszczegolnych klawiszy
					switch (event.key.code) {
						case sf::Keyboard::Escape:
							window.close();
							break;
						case sf::Keyboard::F:
							state = CHOSING_DRAW_COLOR;
							break;
						case sf::Keyboard::B:
							state = CHOSING_BG_COLOR;
							break;
						case sf::Keyboard::L:
							state = DRWNG_LINE;
							break;
						case sf::Keyboard::R:
							state = DRWNG_RECT;
							break;
						case sf::Keyboard::A:
							state = DRWNG_RECT_FILL;
							break;
						case sf::Keyboard::C:
							state = DRWNG_CRCL;
							break;
						case sf::Keyboard::W:
							if(!save_image(buffer)) {
								std::cerr << "Saving file unsuccessfull!\n";
							}
							break;
						case sf::Keyboard::O:
							if(!load_image(buffer)) {
								std::cerr << "Loading file unsuccessfull!\n";
							}
							break;
						default:
							break;
					}

					break;
				// obsluga wcisniecia klawisza myszki
				case sf::Event::MouseButtonPressed:{
					int x = event.mouseButton.x;
					int y = event.mouseButton.y;
					if((state == CHOSING_BG_COLOR || CHOSING_DRAW_COLOR) && ((x > 1) && (y > 1) && (x < 765) && (y < 62))){
						sf::Color col = menu.get_palette_color(x, y);
						switch (state){
							case CHOSING_DRAW_COLOR:{
								menu.set_fg_color(col);
								ds.set_draw_color(col);
								break;
							}case CHOSING_BG_COLOR:{
								menu.set_bg_color(col);
								ds.set_back_color(col);
								break;
							}default:
								break;
						}
						state = IDLE;
					}
					if(state > 2){
						ds.set_mode(state);
						drawing = true;
					}
					mouse_drag_X = event.mouseButton.x;
					mouse_drag_Y = event.mouseButton.y;
					break;
				}
				// poruszenie myszka
				case sf::Event::MouseMoved:
					mouseX = event.mouseMove.x;
					mouseY = event.mouseMove.y;

					break;
				// uzytkownik przestal naciskac przycisk na myszce
				case sf::Event::MouseButtonReleased:
					state = IDLE;

					break;
				// zamkniecie okna, na przyklad przyciskiem na pasku okna
				case sf::Event::Closed:
					window.close();

					break;
				default:
					break;
			}
		}

		window.clear(sf::Color::Transparent);

		if(!drawing) {
			window.draw( sf::Sprite{ buffer.getTexture() } );
		} else {
			if(state > 2){
				window.draw( sf::Sprite{ buffer.getTexture() } );
				window.draw(ds);
			}else{
				buffer.draw(ds);
				window.draw( sf::Sprite{ buffer.getTexture() } );
				ds.set_mode(state);
				drawing = false;
			}
		}
		
		window.draw(menu);
		window.display();
	}
	return 0;
}


bool save_image(const sf::RenderTexture &b){

    const sf::Image image{ b.getTexture().copyToImage() };
    return image.saveToFile("screenshot.png");

}
bool load_image(sf::RenderTexture &b){

    sf::Texture tex;
    if(tex.loadFromFile("screenshot.png")) {
        sf::Sprite spr{tex};
        b.draw(spr);
        return true;
    }
    return false;

}