#include "../include/menu.hpp"

#include "../fontdata/font.h"

Menu::Menu(CURRENT_ACTION* s, const sf::Color &front, const sf::Color &back):state{s} {
		font.loadFromMemory(font_data, font_data_size);
		text = new sf::Text;
		text->setFont(font);
		text->setCharacterSize(12);
		text->setFillColor(sf::Color::White);

		rectangle = new sf::RectangleShape(sf::Vector2f(796, 536));
		rectangle->setFillColor(sf::Color::Transparent);
		rectangle->setOutlineColor(sf::Color::White);
		rectangle->setOutlineThickness(1.0f);
		rectangle->setPosition(2, 62);

		rectangle_text_background = new sf::RectangleShape{sf::Vector2f{800, 51}};
		rectangle_text_background -> setFillColor(sf::Color::Black);
		rectangle_text_background -> setOutlineColor(sf::Color::Black);
		rectangle_text_background -> setPosition(sf::Vector2f{0, 599});
		rectangle_colors_background = new sf::RectangleShape{sf::Vector2f{800, 51}};
		rectangle_colors_background -> setFillColor(sf::Color::Black);
		rectangle_colors_background -> setOutlineColor(sf::Color::Black);
		rectangle_colors_background -> setPosition(sf::Vector2f{0, 0});

		fg_color_indicator = new sf::RectangleShape{sf::Vector2f{30, 30}};
		fg_color_indicator -> setFillColor(sf::Color::Black);
		fg_color_indicator -> setOutlineColor(sf::Color::Black);
		fg_color_indicator -> setOutlineThickness(1.0f);
		fg_color_indicator -> setPosition(768, 2);
		
		bg_color_indicator = new sf::RectangleShape{sf::Vector2f{30, 30}};
		bg_color_indicator -> setFillColor(sf::Color::Black);
		bg_color_indicator -> setOutlineColor(sf::Color::Black);
		bg_color_indicator -> setOutlineThickness(1.0f);
		bg_color_indicator -> setPosition(768, 32);

		set_bg_color(back);		set_fg_color(front);

		unsigned int x, y;
		colors_pixels = new sf::Uint8[colors_size_x * colors_size_y * 4];
		for (x = 0; x<255; x++)
			for (y = 0; y < 30; y++) {
				draw_to_color_pixels(x, y, x, 255, 0);
				draw_to_color_pixels(x+255, y, 255, 255-x, 0);
				draw_to_color_pixels(x + 510, y, 255, 0, x);
				draw_to_color_pixels(254 - x, y+30, 0, 255, 255-x);
				draw_to_color_pixels(509 - x, y + 30, 0, x, 255 );
				draw_to_color_pixels(764 - x, y + 30, 255-x, 0, 255);
			}

		colors_texture = new sf::Texture();
		colors_texture->create(colors_size_x, colors_size_y);
		colors_texture->update(colors_pixels);

		colors_sprite = new sf::Sprite();
		colors_sprite->setTexture(*colors_texture);
		colors_sprite->setPosition(1, 1);
	}


void Menu::draw(sf::RenderTarget& target, sf::RenderStates states) const {

	target.draw(*rectangle_text_background);

	outtextxy(target, 5, 600, L"f - wybor koloru rysowania");
	outtextxy(target, 5, 615, L"b - wybor koloru wypelniania");
	outtextxy(target, 5, 630, L"l - rysowanie linii");
	outtextxy(target, 200, 600, L"r - rysowanie prostokata");
	outtextxy(target, 200, 615, L"a - rysowanie wypelnionego prostokata");
	outtextxy(target, 200, 630, L"c - rysowanie okregu");
	outtextxy(target, 470, 600, L"w - zapis do pliku");
	outtextxy(target, 470, 615, L"o - odczyt z pliku");
	outtextxy(target, 470, 630, L"esc - wyjscie");
	outtextxy(target, 650, 615, L"Aktualne:");
    
	outtextxy(target, 705, 615, std::string{" fbcral"[*state]});

	target.draw(*rectangle_colors_background);
	target.draw(*fg_color_indicator);
	target.draw(*bg_color_indicator);
	target.draw(*rectangle);
	target.draw(*colors_sprite);
    
}