#ifndef _MENU_HPP_
#define _MENU_HPP_

#include<iostream>

#include<SFML/Window.hpp>
#include<SFML/Graphics.hpp>

#include "utility.hpp"


class Menu : public sf::Drawable {
private:
	sf::Font font;
	sf::Text *text;
	sf::RectangleShape *rectangle;
	sf::RectangleShape *rectangle_text_background;
	sf::RectangleShape *rectangle_colors_background;
	sf::RectangleShape *fg_color_indicator;
	sf::RectangleShape *bg_color_indicator;

	sf::Texture *colors_texture;
	sf::Sprite *colors_sprite;
	sf::Uint8 *colors_pixels;
	CURRENT_ACTION* state;
	const unsigned int colors_size_x = 765;
	const unsigned int colors_size_y = 60;
	inline void draw_to_color_pixels(unsigned int x, unsigned int y,unsigned char r, unsigned char g, unsigned char b) {
		colors_pixels[4 * (y * colors_size_x + x) + 0] = r;
		colors_pixels[4 * (y * colors_size_x + x) + 1] = g;
		colors_pixels[4 * (y * colors_size_x + x) + 2] = b;
		colors_pixels[4 * (y * colors_size_x + x) + 3] = 255;
	}
	void outtextxy(sf::RenderTarget& target, float x, float y, const wchar_t* str) const {
		text->setPosition(x, y); 
		text->setString(str); 
		target.draw(*text);
	}
	void outtextxy(sf::RenderTarget& target, float x, float y, const std::string& str) const {
		text->setPosition(x, y); 
		text->setString(str); 
		target.draw(*text);
	}

public:
	Menu(CURRENT_ACTION* s, const sf::Color& = sf::Color::White, const sf::Color& = sf::Color::White);

	// probkowanie koloru znajdujacego sie w danej pozycji na palecie kolorow
	sf::Color get_palette_color(int x, int y) const {
		sf::Color ret;
		ret.r = colors_pixels[4 * ((y-2) * colors_size_x + (x-2)) + 0];
		ret.g = colors_pixels[4 * ((y-2) * colors_size_x + (x-2)) + 1];
		ret.b = colors_pixels[4 * ((y-2) * colors_size_x + (x-2)) + 2];
		return ret;
	}
	// metody ustawiajace kolor wskaznikow koloru
	void set_fg_color(const sf::Color &col) {
		fg_color_indicator -> setFillColor(col);
	}
	void set_bg_color(const sf::Color &col) {
		bg_color_indicator -> setFillColor(col);
	}

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};


#endif