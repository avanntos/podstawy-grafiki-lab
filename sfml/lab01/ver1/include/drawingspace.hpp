#ifndef _DRAWINGSPACE_HPP_
#define _DRAWINGSPACE_HPP_

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "utility.hpp"

class DrawingSpace : public sf::Drawable {
private:
	int *mouseX, *mouseY;
	int *mouse_dragX, *mouse_dragY;
	CURRENT_ACTION drawing_mode;
	sf::Color draw_color;
	sf::Color back_color;
public:
	DrawingSpace(int* x, int* y,int* drx, int* dry):mouseX{x},mouseY{y},mouse_dragX{drx},mouse_dragY{dry}{
		drawing_mode = IDLE;
	}

	// metody ustawiajace kolory ktore sa aktualnie uzywane
	void set_draw_color(const sf::Color &col){	draw_color = col;		}
	void set_back_color(const sf::Color &col){	back_color = col;		}
	// tryb rysowania, uzywany do decyzji co danym momencie rysowac
	void set_mode(const CURRENT_ACTION &mode){	drawing_mode = mode;	}

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const ;
};



#endif