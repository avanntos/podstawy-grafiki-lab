#ifndef _UTILITY_HPP_
#define _UTILITY_HPP_

#include<cmath>

enum CURRENT_ACTION {   IDLE, CHOSING_DRAW_COLOR, CHOSING_BG_COLOR,
                        DRWNG_CRCL, DRWNG_RECT, DRWNG_RECT_FILL, DRWNG_LINE, DRWNG_POINT };

float dist(float,float);

float map(float v, float e1, float e2, float m1, float m2);

#endif