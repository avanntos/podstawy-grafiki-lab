#ifndef _SLIDER_HPP_
#define _SLIDER_HPP_

#include<iostream>
// #include<functional>
// #include<memory>

#include<SFML/Window.hpp>
#include<SFML/Graphics.hpp>

#include"utility.hpp"

class Slider : public sf::Drawable {
private:
    int x, y;
    int w, h;
    float level;
    int* mouseX, *mouseY;
    bool mouse_locked;

    std::unique_ptr<sf::Texture> text;
    std::unique_ptr<sf::RectangleShape> rect;
    std::unique_ptr<sf::VertexArray> line;

    bool check_bounds() {
        return !((*mouseX > (x + w)) || (*mouseX < x) || (*mouseY > (y + h)) || (*mouseY < y));}
    void set_level(){
        if(*mouseY > (y+h)) {
            level = 1;
        } else if (*mouseY < y) {
            level = 0;
        } else {
            level = map(*mouseY, y, y+h, 0, 1);
        }
        (*line)[0].position.y = y + h * level;
        (*line)[1].position.y = y + h * level;
    }
public:
    Slider(int posx, int posy, int _w, int _h): x{posx}, y{posy}, w{_w}, h{_h},
                                                rect{new sf::RectangleShape{sf::Vector2f{w, h}}},
                                                line{new sf::VertexArray{sf::LinesStrip, 2}},
                                                level{.5f}, mouse_locked{false} {
        rect->setPosition(x, y);
        rect->setFillColor(sf::Color::Blue);
        /// init
        (*line)[0].color = sf::Color::Yellow;
        (*line)[1].color = sf::Color::Yellow;
        (*line)[0].position.x = x -     .1*w;
        (*line)[1].position.x = x + w + .1*w;
        (*line)[0].position.y = y + h*level;
        (*line)[1].position.y = y + h*level;
    }

    float get_val() {                   return 1-level;                                     }
    void syncmouse(int* mx, int* my) {  mouseX = mx;                mouseY = my;            }
    void mouse_click() {                if(check_bounds())          mouse_locked = true;    }
    void mouse_release() {              mouse_locked = false;                               }
    bool mouse_move(){
        if( mouse_locked ) {
            set_level();
            return true;
        }
        return false;
    }
    

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

};

#endif