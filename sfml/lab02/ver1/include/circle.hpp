#ifndef _CIRCLE_HPP_
#define _CIRCLE_HPP_

#include<iostream>
#include<cmath>

#include<SFML/Window.hpp>
#include<SFML/Graphics.hpp>

#include<utility.hpp>

class Circle : public sf::Drawable {
private:
    const int r = 150;
    float current_level;

    std::unique_ptr<sf::Uint8[]> circle;
    std::unique_ptr<float[]> angles;
    std::unique_ptr<sf::Texture> texture;

    std::unique_ptr<sf::Sprite> sprite;
public:
    Circle(int posx, int posy):texture{new sf::Texture{}},current_level{.5f}{
        circle = std::make_unique<sf::Uint8[]>(4*r*r*4);
        angles = std::make_unique<float[]>(4*r*r);
        sprite = std::make_unique<sf::Sprite>();
        sprite->setPosition(sf::Vector2f{posx,posy});
    }

    void init() {
        for(int i=0; i<(4*r*r); ++i) {

            int x = -r + i%(2*r);
            int y =  r - i/(2*r);
            int R = dist(x, y);
            if(R <= r-1) {
                angles[i] = atan2f(y, x);

                *(circle.get() + i*4 + 0) = map(R, 0, r, 0, 255);
                *(circle.get() + i*4 + 1) = map(angles[i], 0, 2 * M_PI, 0, 255);
                *(circle.get() + i*4 + 2) = 128;
                *(circle.get() + i*4 + 3) = 255;
            } else {
                *(circle.get() + i*4 + 3) = 0;
            }

        }
        texture->create(2*r, 2*r);
        texture->update(circle.get());

        sprite->setTexture(*(texture.get()));
    }

    void set_val(float b) {
        if(fabs(current_level - b) > 1./255){
            current_level = b;
            for(int i=0; i<(4*r*r); ++i) {

                int x = -r + i%(2*r);
                int y =  r - i/(2*r);

                int R = x*x + y*y;
                // int R = dist(x, y);
                if(R <= r*r-1) {
                    *(circle.get() + i*4 + 2) = 255 * b;
                }

            }
            texture->create(2*r, 2*r);
            texture->update(circle.get());

            sprite->setTexture(*(texture.get()));
        }
    }

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override {
        target.draw(*sprite.get());
    }
};

#endif