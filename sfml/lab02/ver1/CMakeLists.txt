cmake_minimum_required(VERSION 2.6)

project(lab
		DESCRIPTION "very good project"
		LANGUAGES CXX)


set( OUTPUT_NAME exec )
set( COMPILE_FLAGS "-Wall" "-g" )
set( INCLUDE_DIRECTORY ${PROJECT_SOURCE_DIR}/include )
set( SOURCES_DIRECTORY ${PROJECT_SOURCE_DIR}/source )

find_package(SFML 2.5 COMPONENTS network audio graphics window system REQUIRED)

include_directories( include )
file( GLOB SOURCES *.cpp "source/*.cpp" )
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
add_compile_options (${COMPILE_FLAGS})
add_executable(${OUTPUT_NAME} ${SOURCES})
target_link_libraries(${OUTPUT_NAME} sfml-network sfml-audio sfml-graphics sfml-window sfml-system)
add_custom_target( run ./${OUTPUT_NAME})
add_custom_target( valgrind @valgrind ./${OUTPUT_NAME} --tool=memcheck ./${OUTPUT_NAME})