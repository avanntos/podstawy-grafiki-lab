#include "../include/utility.hpp"

float dist(float a, float b){
    return sqrt(a*a + b*b);
}

float map(float v, float e1, float e2, float m1, float m2){
    float ret;

    if(e2 == e1)
        ret = 0;
    else{
        ret=(m2 - m1)*(v - e1)/(e2 - e1) + m1;
    }
    return ret;
}