#include<iostream>

#include<SFML/Window.hpp>
#include<SFML/Graphics.hpp>

#include"../include/slider.hpp"
#include"../include/circle.hpp"

int main() {
    sf::RenderWindow window{sf::VideoMode{900, 600}, "slidertest", sf::Style::Titlebar | sf::Style::Close};
    window.setFramerateLimit(60);

    sf::Event event;

    int mouseX, mouseY;    

    Slider s{200, 40, 60, 200};
    Circle c{300,300};
    c.init();
    s.syncmouse(&mouseX, &mouseY);
    while (window.isOpen()) {
        window.clear(sf::Color::Black);

        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:{
                    window.close();
                }
                case sf::Event::KeyPressed:{
                    switch (event.key.code) {
                    case sf::Keyboard::Escape:
                        window.close();
                        break;
                    default:
                        break;
                    }
                }
                case sf::Event::MouseMoved:{
                    mouseX = event.mouseMove.x;
                    mouseY = event.mouseMove.y;
                    if(s.mouse_move()) {
                        c.set_val(s.get_val());
                    }

                    break;
                }
                case sf::Event::MouseButtonPressed:{
                    s.mouse_click();
                    break;
                }
                case sf::Event::MouseButtonReleased:{
                    s.mouse_release();
                    break;
                }
                default:
                    break;
            }
        }
        window.draw(c);
        window.draw(s);
        window.display();
    }



    return 0;
}