#include"../include/slider.hpp"

void Slider::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    target.draw(*rect);
    target.draw(*line);
}
