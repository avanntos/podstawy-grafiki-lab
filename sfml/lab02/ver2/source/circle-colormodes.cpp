#include"../include/circle-colormodes.hpp"

/***************************HSL***************************/

const sf::Color HSL::mode2rgb(float h, float s, float l) const {

    if(s == 0) {

        sf::Uint8 R = l*255;
        sf::Uint8 G = l*255;
        sf::Uint8 B = l*255;
        return sf::Color{R, G, B};
    } else {

        float var_2 = (l < .5f) ? (l*(1 + s)):((s + l) - (s*l));
        float var_1 = 2*l - var_2;

        sf::Uint8 R = 255 * hue2rgb(var_1, var_2, h + (1.f/3));
        sf::Uint8 G = 255 * hue2rgb(var_1, var_2, h );
        sf::Uint8 B = 255 * hue2rgb(var_1, var_2, h - (1.f/3));
        return sf::Color{R, G, B};
    }

}

float HSL::hue2rgb(float p, float q, float h) const {
    if(h < 0.f)
        h += 1.f;

    if(h > 1.f)
        h -= 1.f;

    if(h < (1.f/6))
        return (p + (q - p)*6.f*h);
    if( h < (.5f))
        return q;
    if(h < (2.f/3))
        return (p + (q - p)*((2.f/3) - h )*6);
    return p;
}

/***************************HSV***************************/

const sf::Color HSV::mode2rgb(float h, float s, float v) const {

    if (s == 0) {
        sf::Uint8 R = v*255;
        sf::Uint8 G = v*255;
        sf::Uint8 B = v*255;
        return sf::Color{R, G, B};
    } else {
        float p = h*6;

        if (p == 6)
            p = 0;

        int q = (int)p;
        float val1 = v*(1 - s);
        float val2 = v*(1 - s*(p - q));
        float val3 = v*(1 - s*(1 - (p - q)));

        float R, G, B;
        switch(q){
            case 0:
                R = v;
                G = val3;
                B = val1;
                break;
            case 1:
                R = val2;
                G = v;
                B = val1;
                break;
            case 2:
                R = val1;
                G = v;
                B = val3;
                break;
            case 3:
                R = val1;
                G = val2;
                B = v;
                break;
            case 4:
                R = val3;
                G = val1;
                B = v;
                break;
            case 5:
                R = v;
                G = val1;
                B = val2;
                break;
            default:
                R = G = B = 0;
                break;
        }

        return sf::Color{static_cast<sf::Uint8>(R*255), static_cast<sf::Uint8>(G*255), static_cast<sf::Uint8>(B*255)};
    }
}