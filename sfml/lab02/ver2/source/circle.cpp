#include"../include/circle.hpp"


Circle::Circle(int posx, int posy, const std::string& name):x{posx},y{posy},current_level{.5f},label{name}{

    // creating and initializing font
    font = std::make_unique<sf::Font>();
    font->loadFromFile("../arial.ttf");

    // creating and initializing label display
    text_label = std::make_unique<sf::Text>();
    text_label->setPosition(static_cast<float>(x), static_cast<float>(y));
    text_label->setFillColor(sf::Color::White);
    text_label->setFont(*font);
    text_label->setCharacterSize(10);
    text_label->setString(label);

    // creating and initializing label display
    text_val = std::make_unique<sf::Text>();
    text_val->setPosition(static_cast<float>(x+r*2 - 30), static_cast<float>(y+r*2 - 10));
    text_val->setFillColor(sf::Color::White);
    text_val->setFont(*font);
    text_val->setCharacterSize(10);
    text_val->setLetterSpacing(1.5f);
}

void Circle::init() {
    // pixel, its distance and angle data
    circle = std::make_unique<sf::Uint8[]>(arr_size*4);
    angles = std::make_unique<double[]>(arr_size);
    distances = std::make_unique<float[]>(arr_size);

    // creating and setting sprite's position
    sprite = std::make_unique<sf::Sprite>();
    sprite->setPosition(sf::Vector2f{static_cast<float>(x), static_cast<float>(y)});
    
    // generating pixels' colors' values later passed into texture
    for(int i=0; i<(4*r*r); ++i) {

        int x = -r + i%(2*r);
        int y =  r - i/(2*r);
        float R = dist(x, y);
        if(R <= (r-1)) {
            distances[i] = map(R, 0, r, 0, 1);
            angles[i] = map(atan2f(y, x), -M_PI, M_PI, 0 , 1);
            sf::Color c = mode2rgb(angles[i], distances[i], current_level);

            *(circle.get() + i*4 + 0) = c.r;
            *(circle.get() + i*4 + 1) = c.g;
            *(circle.get() + i*4 + 2) = c.b;
            *(circle.get() + i*4 + 3) = 255;
        } else {
            distances[i] = -1;
            *(circle.get() + i*4 + 3) = 0;
        }

    }

    /// creating texture and updating it with generated pixel values
    texture = std::make_unique<sf::Texture>();
    texture->create(2*r, 2*r);
    texture->update(circle.get());
    sprite->setTexture(*texture);

}