#include<iostream>

#include<SFML/Window.hpp>
#include<SFML/Graphics.hpp>

#include"../include/slider.hpp"
#include"../include/circle.hpp"
#include"../include/circle-colormodes.hpp"

int main() {
    sf::RenderWindow window{sf::VideoMode{800, 650}, "slidertest", sf::Style::Titlebar | sf::Style::Close};
    // window.setFramerateLimit(60);


    int mouseX, mouseY;    

    Slider s{600, 200, 30, 245};

    HSL hsl{100, 100, "HSL"};
    hsl.init();
    HSV hsv{350, 100, "HSV"};
    hsv.init();
    CMY cmy{100, 300 + 50, "CMY"};
    cmy.init();
    RGB rgb{350, 300 + 50, "RGB"};
    rgb.init();

    s.syncmouse(&mouseX, &mouseY);
    /**************initlializing clock*****************/
    sf::Clock clock;
    unsigned int FPS = 0, frame_counter = 0;
    
    clock.restart().asMilliseconds();
    /**************loading font and initializing text printing***********/
    sf::Font font;
    font.loadFromFile("../arial.ttf");
    sf::Text text_fps;
    text_fps.setFont(font);
    text_fps.setCharacterSize(15);
    text_fps.setLetterSpacing(1.5f);
    // text_fps.setPosition(.0f, .0f);
    text_fps.setPosition(580.f, 465.f);
    text_fps.setFillColor(sf::Color::White);

    std::string current_fps;
    sf::Event event;
    while (window.isOpen()) {
        window.clear(sf::Color::Black);

        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:{
                    window.close();
                }
                case sf::Event::KeyPressed:{
                    switch (event.key.code) {
                    case sf::Keyboard::Escape:
                        window.close();
                        break;
                    default:
                        break;
                    }
                }
                case sf::Event::MouseMoved:{
                    mouseX = event.mouseMove.x;
                    mouseY = event.mouseMove.y;
                    if(s.mouse_move()) {
                        float v = s.get_val();
                        hsl.set_val(v);
                        hsv.set_val(v);
                        cmy.set_val(v);
                        rgb.set_val(v);
                    }

                    break;
                }
                case sf::Event::MouseButtonPressed:{
                    s.mouse_click();
                    break;
                }
                case sf::Event::MouseButtonReleased:{
                    s.mouse_release();
                    break;
                }
                default:
                    break;
            }
        }
        /**********************************************/
        if(clock.getElapsedTime().asSeconds() >= 1.0f) {
            FPS = (unsigned int)((float)frame_counter/clock.getElapsedTime().asSeconds());
            clock.restart();
            frame_counter = 0;
        }
        frame_counter++;

        current_fps = "FPS: " + std::to_string(FPS);
        text_fps.setString(current_fps);
        /**********************************************/
        window.draw(hsl);
        window.draw(hsv);
        window.draw(cmy);
        window.draw(rgb);
        window.draw(s);
        window.draw(text_fps);
        window.display();
    }



    return 0;
}