#include"../include/slider.hpp"

Slider::Slider(int _x, int _y, int _w, int _h): x{_x}, y{_y}, w{_w}, h{_h},
                                            level{.5f}, mouse_locked{false}, 
                                            line{new sf::VertexArray{sf::LinesStrip, 2}}{
        
    // initializing line serving as indicator for the slider init
    (*line)[0].color = sf::Color::Red;
    (*line)[1].color = sf::Color::Red;
    (*line)[0].position.x = x -     .1*w;
    (*line)[1].position.x = x + w + .1*w;
    (*line)[0].position.y = y + h*level;
    (*line)[1].position.y = y + h*level;

    // creating array storing color values for background
    std::unique_ptr<sf::Uint8[]> points = std::make_unique<sf::Uint8[]>(w*h*4);
    for(int i=0; i<h; ++i) {
        float t = ((float)i)/(h-1);
        // making black-to-white gradient a little bit more prominent
        t=t*t;
        sf::Uint8 c = map(t, 0, 1, 0, 255);
        for(int j=0; j<w; ++j) {
            // if((i == 0) || (i == (h-1)) || (j == 0) ||( j == (w-1)))
            if((j == 0) || (j == (w-1))){
                *(points.get() + (i*w + j)*4 + 0) = 255;
                *(points.get() + (i*w + j)*4 + 1) = 255;
                *(points.get() + (i*w + j)*4 + 2) = 255;
            } else {
                *(points.get() + (i*w + j)*4 + 0) = c;
                *(points.get() + (i*w + j)*4 + 1) = c;
                *(points.get() + (i*w + j)*4 + 2) = c;
            }
            *(points.get() + (i*w + j)*4 + 3) = 255;
        }
    }

    // creating texture and updating it with created array of colors
    texture = std::make_unique<sf::Texture>();
    texture->create(w,h);
    texture->update(points.get());

    // creating drawable sprite with background texture
    background = std::make_unique<sf::Sprite>();
    background->setPosition(sf::Vector2f{static_cast<float>(x), static_cast<float>(y)});
    background->setTexture(*texture);
}

void Slider::set_level(){
    if(*mouseY > (y+h)) {
        level = 1;
    } else if (*mouseY < y) {
        level = 0;
    } else {
        level = map(*mouseY, y, y+h, 0, 1);
    }
    (*line)[0].position.y = y + h * level;
    (*line)[1].position.y = y + h * level;
}

void Slider::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    target.draw(*background);
    target.draw(*line);
}