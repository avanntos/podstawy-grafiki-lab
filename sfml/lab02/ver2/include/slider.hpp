#ifndef _SLIDER_HPP_
#define _SLIDER_HPP_

#include<iostream>

#include<SFML/Window.hpp>
#include<SFML/Graphics.hpp>

#include"utility.hpp"

/// simple implementation of slider, sufficient enough for this usecase
class Slider : public sf::Drawable {
private:
    int x, y;
    int w, h;
    float level;
    int* mouseX, *mouseY;
    bool mouse_locked;

    std::unique_ptr<sf::Texture> texture;
    std::unique_ptr<sf::Sprite> background;

    std::unique_ptr<sf::VertexArray> line;

    /// method checking if mouse pointer is within slider's boundaries
    bool check_bounds() {
        return !((*mouseX > (x + w)) || (*mouseX < x) || (*mouseY > (y + h)) || (*mouseY < y));}

    /// sets slider's level based on mouse position
    void set_level();
public:
    /// parameters: position of a slider X,Y, with of slider W,H
    Slider(int _x, int _y, int _w, int _h);

    /// returns slider's current value
    float get_val() {
        return level; }

    /// assigns mouseX and mouseY to their values in main, can skip refreshing them each frame
    void syncmouse(int* mx, int* my) {
        mouseX = mx;
        mouseY = my; }

    /// checks if click happened within slider's boundaries, in which case it will start tracking it's movement
    void mouse_click() {
        if(check_bounds())
            mouse_locked = true; }

    /// stops tracking of mouse pointer
    void mouse_release() {
        mouse_locked = false; }

    /// invokes method changing slider's value if mouse pointer moved and is being tracked
    bool mouse_move() {
        if( mouse_locked ) {
            set_level();
            return true;
        }
        return false;
    }
    

    virtual void draw(sf::RenderTarget&, sf::RenderStates) const override;

};

#endif