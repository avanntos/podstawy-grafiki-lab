#ifndef _CIRCLE_HPP_
#define _CIRCLE_HPP_

#include<iostream>
#include<cmath>

#include<SFML/Window.hpp>
#include<SFML/Graphics.hpp>

#include<utility.hpp>

/// base class for drawing and updating circle
class Circle : public sf::Drawable {
protected:
    int x, y;
    const int r = 100;
    const int arr_size = r*r*4;
    float current_level;

    const std::string label;
    std::string screen_val;

    std::unique_ptr<sf::Uint8[]> circle;
    std::unique_ptr<double[]> angles;
    std::unique_ptr<float[]> distances;

    std::unique_ptr<sf::Texture> texture;
    std::unique_ptr<sf::Sprite> sprite;

    std::unique_ptr<sf::Font> font;
    std::unique_ptr<sf::Text> text_label;
    std::unique_ptr<sf::Text> text_val;

    /**************************************/

    /// initializing text displaying value of color component we manipulate
    virtual void init_screen_val() = 0;

    /// updates text displaying value of color component we manipulate
    virtual void update_screen_val() = 0;

    /// method in which each deriving class implements their own color transformation
    virtual const sf::Color mode2rgb(float,float,float) const = 0;

    /// method in which each deriving class updates their pixel data
    virtual void recalc_colors() = 0;

public:
    /// arguments: position x and position y of the wheel, label displayed next to it
    Circle(int posx, int posy, const std::string& name);

    /// initializing circle's starting values, relatively heavy operations are moved here from constructor
    void init();

    /// interface for changing circle's colors, passed argument should be from range <0-1>
    void set_val(float b) {
        if(fabs(current_level - b) > (1./255)){
            current_level = b;

            recalc_colors();

            texture->update(circle.get());
            sprite->setTexture(*(texture.get()));

            update_screen_val();
            text_val->setString(screen_val);
        }
    }

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override {
        target.draw(*sprite.get());
        target.draw(*text_label);
        target.draw(*text_val);
    }

};

#endif