#ifndef _UTILITY_HPP_
#define _UTILITY_HPP_

#include<cmath>

/// helper function calculating euclidean distance
float dist(float,float);

/// helper function mapping value "v", ranging <e1,e2> into range <m1,m2>
float map(float v, float e1, float e2, float m1, float m2);

#endif