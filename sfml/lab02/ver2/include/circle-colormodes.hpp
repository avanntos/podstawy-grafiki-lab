#ifndef _C_COLORMODES_
#define _C_COLORMODES_

#include<SFML/Window.hpp>
#include<SFML/Graphics.hpp>

#include"circle.hpp"

#include"utility.hpp"

/// HSL, HSV, CMY and RGB are classes inheriting from base class Circle, each implements its own methods for
/// tuning initialization and updating colors of circle

class HSL : public Circle {
    public:
    HSL(int x, int y, const std::string& name):Circle{x, y, name}{
        // initializing value display string
        init_screen_val();
        update_screen_val();
        text_val->setString(screen_val);
    }

    virtual void recalc_colors() {
        for(int i=0; i<arr_size; ++i) {
            if(distances[i] > -1) {
                sf::Color c{mode2rgb(angles[i], distances[i], current_level)};
                *(circle.get() + i*4 + 0) = c.r;
                *(circle.get() + i*4 + 1) = c.g;
                *(circle.get() + i*4 + 2) = c.b;
            }
        }
    }
    virtual const sf::Color mode2rgb(float, float, float) const override;

    float hue2rgb(float, float, float) const;

    virtual void init_screen_val() override {
        screen_val.resize(8);
        screen_val[0] = label[2];
        screen_val[1] = '=';
        screen_val[3] = '.';
        screen_val[7] = '\n';
    }

    virtual void update_screen_val() override {
        screen_val[2] = static_cast<char>(48 + ((int)(current_level*1   ))%10);
        screen_val[4] = static_cast<char>(48 + ((int)(current_level*10  ))%10);
        screen_val[5] = static_cast<char>(48 + ((int)(current_level*100 ))%10);
        screen_val[6] = static_cast<char>(48 + ((int)(current_level*1000))%10);
    }
};

class HSV : public Circle {
    public:
    HSV(int x, int y, const std::string& name):Circle{x, y, name}{
        // initializing value display string
        init_screen_val();
        update_screen_val();
        text_val->setString(screen_val);
    }

    virtual void recalc_colors() {
        for(int i=0; i<arr_size; ++i) {
            if(distances[i] > -1) {
                sf::Color c{mode2rgb(angles[i], distances[i], current_level)};
                *(circle.get() + i*4 + 0) = c.r;
                *(circle.get() + i*4 + 1) = c.g;
                *(circle.get() + i*4 + 2) = c.b;
            }
            
        }
    }

    virtual const sf::Color mode2rgb(float H, float S, float V) const override;

    virtual void init_screen_val() override {
        screen_val.resize(8);
        screen_val[0] = label[2];
        screen_val[1] = '=';
        screen_val[3] = '.';
        screen_val[7] = '\n';
    }

    virtual void update_screen_val() override {
        screen_val[2] = static_cast<char>(48 + ((int)(current_level*1   ))%10);
        screen_val[4] = static_cast<char>(48 + ((int)(current_level*10  ))%10);
        screen_val[5] = static_cast<char>(48 + ((int)(current_level*100 ))%10);
        screen_val[6] = static_cast<char>(48 + ((int)(current_level*1000))%10);
    }

};

class CMY : public Circle {
    public:
    CMY(int x, int y, const std::string& name):Circle{x, y, name}{
        // initializing value display string
        init_screen_val();
        update_screen_val();
        text_val->setString(screen_val);
    }

    virtual void recalc_colors() {
        for(int i=0; i<arr_size; ++i) {
            if(distances[i] > -1) {
                sf::Color c{mode2rgb(angles[i], distances[i], current_level)};
                *(circle.get() + i*4 + 0) = c.r;
                *(circle.get() + i*4 + 1) = c.g;
                *(circle.get() + i*4 + 2) = c.b;
            }
            
        }
    }

    virtual const sf::Color mode2rgb(float g, float r, float b) const override {
        return sf::Color{   static_cast<sf::Uint8>((1-r) * 255),
                            static_cast<sf::Uint8>((1-g) * 255),
                            static_cast<sf::Uint8>((1-b) * 255)};
    }

    virtual void init_screen_val() override {
        screen_val.resize(7);
        screen_val[0] = label[2];
        screen_val[1] = '=';
        screen_val[6] = '\n';

        update_screen_val();
    }

    virtual void update_screen_val() override {

        char c = static_cast<char>(48 + ((int)(current_level*1   ))%10);
        screen_val[2] = (c == '0')?(' '):(c);

        char d = static_cast<char>(48 + ((int)(current_level*10  ))%10);
        screen_val[3] = (d == '0' && current_level != 1.f)?(' '):(d);

        screen_val[4] = static_cast<char>(48 + ((int)(current_level*100 ))%10);
        screen_val[5] = static_cast<char>(37);
    }
};

class RGB : public Circle{
    public:
    RGB(int x, int y, const std::string& name):Circle{x, y, name}{
        // initializing value display string
        init_screen_val();
        update_screen_val();
        text_val->setString(screen_val);
    }

    virtual void recalc_colors() {
        for(int i=0; i<arr_size; ++i) {
            if(distances[i] > -1) {
                *(circle.get() + i*4 + 2) = 255 * current_level;
            }
        }
    }
    virtual const sf::Color mode2rgb(float g, float r, float b) const override {
        return sf::Color{   static_cast<sf::Uint8>(255 * r),
                            static_cast<sf::Uint8>(255 * g),
                            static_cast<sf::Uint8>(255 * b)};
    }

    virtual void init_screen_val() override {
        screen_val.resize(7);
        screen_val[0] = label[2];
        screen_val[1] = '=';
        screen_val[6] = '\n';

        update_screen_val();
    }

    virtual void update_screen_val() override {

        char c = static_cast<char>(48 + ((int)(current_level*1   ))%10);
        screen_val[2] = (c == '0')?(' '):(c);

        char d = static_cast<char>(48 + ((int)(current_level*10  ))%10);
        screen_val[3] = (d == '0' && current_level != 1.f)?(' '):(d);

        screen_val[4] = static_cast<char>(48 + ((int)(current_level*100 ))%10);
        screen_val[5] = static_cast<char>(37);
    }
};

#endif